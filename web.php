<?php

use Illuminate\Support\Facades\Route;
#asda
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello',function(){
    return 'Hello Laravel';
});

Route::get('/student/{id}',function($id='No student found'){
    return 'We got student with id'.$id;
});

Route::get('/car/{id?}',function($id=null){
    if(isset($id)){
        return "We got car $id";
    }else{
        return 'We need id to find your car';
    }
   
});

Route::get('/comment/{id}', function ($id) {
    return view('comment',compact('id'));
});

Route::get('/users/{email}/{name?}',function($email='No email found',$name=null){
    if(isset($name)){
        return view('users',compact('email','name'));
    }else{
        $name='name missing';
        return view('users',compact('email','name'));
    }
    
 
});
